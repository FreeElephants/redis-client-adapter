# Redis Client Adapter

Simple wrapper with serialization for PHPRedis extension. 
 
## Installation: 

```
# Add to composer.json next lines:

"require": {
    "free-elephants/redis-client-adapter": "*"
}

"repositories": [
    {
        "type": "vcs",
        "url": "git@bitbucket.org:FreeElephants/redis-client-adapter.git"
    }
]

```