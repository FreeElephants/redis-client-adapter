<?php

namespace FreeElephants\Redis;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
class RedisClient implements RedisClientInterface
{

    /**
     * @var string
     */
    private $host;
    /**
     * @var int
     */
    private $port;
    /**
     * @var null
     */
    private $password;
    /**
     * @var string
     */
    private $dbIndex;

    public function __construct(string $host = '127.0.0.1', int $port = 6379, $password = null, string $dbIndex = null)
    {
        $this->host = $host;
        $this->port = $port;
        $this->password = $password;
        $this->dbIndex = $dbIndex;
    }

    protected function getConnection()
    {
        static $redis;
        if (empty($redis)) {
            {
                $redis = new \Redis();
                $redis->connect($this->host, $this->port);
                if (isset($this->password)) {
                    $redis->auth($this->password);
                }
                if (isset($this->dbIndex)) {
                    $redis->select($this->dbIndex);
                }
            }
        }

        return $redis;
    }

    public function get(string $key)
    {
        $value = $this->getConnection()->get($key);
        if ($value !== false) {
            $value = $this->unserialize($value);
        }
        return $value;
    }

    public function set(string $key, $value, int $ttl = 0): bool
    {
        $data = $this->serialize($value);
        $options = ['ex' => $ttl];
        return $this->getConnection()->set($key, $data, $options);
    }

    public function hSet(string $hashName, string $key, $value): bool
    {
        $value = $this->serialize($value);
        return (bool)$this->getConnection()->hSet($hashName, $key, $value);
    }

    public function hGet(string $hashName, string $key)
    {
        $value = $this->getConnection()->hGet($hashName, $key);
        return $this->unserialize($value);
    }

    public function hExists(string $hashName, string $authKey): bool
    {
        return $this->getConnection()->hExists($hashName, $authKey);
    }

    private function unserialize($value)
    {
        return unserialize($value);
    }

    private function serialize($value): string
    {
        return serialize($value);
    }
}