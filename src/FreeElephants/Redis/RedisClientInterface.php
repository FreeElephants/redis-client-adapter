<?php

namespace FreeElephants\Redis;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
interface RedisClientInterface
{

    public function get(string $key);

    public function set(string $key, $value, int $ttl): bool;

    public function hSet(string $hashName, string $key, $value): bool;

    public function hGet(string $hashName, string $key);

    public function hExists(string $hashName, string $authKey): bool;
}